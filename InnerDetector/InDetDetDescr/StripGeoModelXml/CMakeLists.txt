# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( StripGeoModelXml )

find_package( GeoModelCore )

# Component(s) in the package:
atlas_add_component( StripGeoModelXml
                     src/*.cxx
                     src/components/*.cxx
                     LINK_LIBRARIES GaudiKernel GeoModelUtilities GeoModelXml InDetGeoModelUtils SCT_ReadoutGeometry
                     PRIVATE_LINK_LIBRARIES ${GEOMODELCORE_LIBRARIES} AthenaPoolUtilities DetDescrConditions GeoModelInterfaces InDetReadoutGeometry ReadoutGeometryBase  InDetSimEvent PathResolver RDBAccessSvcLib SGTools StoreGateLib )

# Install files from the package:
atlas_install_python_modules( python/*.py POST_BUILD_CMD ${ATLAS_FLAKE8} )

# Test(s) in the package:
atlas_add_test( ITkStripGMConfig_test
                SCRIPT test/ITkStripGMConfig_test.py
                PROPERTIES TIMEOUT 300 )
